/*  $Id$
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Copyright (C) 2013 Cedric Leporcq  <cedl38@gmail.com>
 *
 */

#ifndef __WCK_PLUGIN_H__
#define __WCK_PLUGIN_H__

#include <xfconf/xfconf.h>
#include <libxfce4panel/libxfce4panel.h>
#include <libxfce4util/libxfce4util.h>

G_BEGIN_DECLS

/* default settings */
#define DEFAULT_ONLY_MAXIMIZED TRUE
#define DEFAULT_SHOW_ON_DESKTOP FALSE

typedef void (*WckSettingsCb) (XfceRc *rc, gpointer prefs);

XfconfChannel *
wck_properties_get_channel (GObject *object_for_weak_ref, const gchar *channel_name);
void wck_about (XfcePanelPlugin *plugin, const gchar *icon_name);
GtkWidget *show_refresh_item (XfcePanelPlugin *plugin);
void wck_settings_save (XfcePanelPlugin *plugin, WckSettingsCb save_settings, gpointer prefs);
void wck_settings_load (XfcePanelPlugin *plugin, WckSettingsCb load_settings, gpointer prefs);
GtkWidget *wck_dialog_get_widget (GtkBuilder *builder, const gchar *name);
void wck_configure_dialog (XfcePanelPlugin *plugin, const gchar *icon_name, GtkWidget *ca, GCallback response_cb, gpointer data);
void wck_configure_response (XfcePanelPlugin *plugin, GtkWidget *dialog, gint response, WckSettingsCb save_settings, gpointer data);

G_END_DECLS

#endif /* __WCK_PLUGIN_H__ */

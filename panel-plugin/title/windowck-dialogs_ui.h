/* automatically generated from windowck-dialogs.glade */
#ifdef __SUNPRO_C
#pragma align 4 (windowck_dialogs_ui)
#endif
#ifdef __GNUC__
static const char windowck_dialogs_ui[] __attribute__ ((__aligned__ (4))) =
#else
static const char windowck_dialogs_ui[] =
#endif
{
  "<?xml version=\"1.0\" encoding=\"UTF-8\"?><interface><requires lib=\"gt"
  "k+\" version=\"3.22\"/><object class=\"GtkListStore\" id=\"size_mode_li"
  "st\"><columns><column type=\"gchararray\"/></columns><data><row><col id"
  "=\"0\" translatable=\"yes\">at most</col></row><row><col id=\"0\" trans"
  "latable=\"yes\">fixed to</col></row><row><col id=\"0\" translatable=\"y"
  "es\">expand</col></row></data></object><object class=\"GtkListStore\" i"
  "d=\"title_alignment_list\"><columns><column type=\"gchararray\"/></colu"
  "mns><data><row><col id=\"0\" translatable=\"yes\">left</col></row><row>"
  "<col id=\"0\" translatable=\"yes\">center</col></row><row><col id=\"0\""
  " translatable=\"yes\">right</col></row></data></object><object class=\""
  "GtkBox\" id=\"vbox0\"><property name=\"visible\">True</property><proper"
  "ty name=\"can-focus\">False</property><property name=\"margin-start\">1"
  "4</property><property name=\"margin-end\">14</property><property name=\""
  "margin-top\">8</property><property name=\"margin-bottom\">14</property>"
  "<property name=\"orientation\">vertical</property><property name=\"spac"
  "ing\">10</property><child><object class=\"GtkFrame\" id=\"contentarea1\""
  "><property name=\"visible\">True</property><property name=\"can-focus\""
  ">False</property><property name=\"label-xalign\">0</property><property "
  "name=\"shadow-type\">none</property><child><object class=\"GtkBox\" id="
  "\"vbox1\"><property name=\"visible\">True</property><property name=\"ca"
  "n-focus\">False</property><property name=\"margin-start\">12</property>"
  "<property name=\"margin-end\">10</property><property name=\"margin-top\""
  ">5</property><property name=\"margin-bottom\">3</property><property nam"
  "e=\"orientation\">vertical</property><property name=\"spacing\">1</prop"
  "erty><child><object class=\"GtkRadioButton\" id=\"only_maximized\"><pro"
  "perty name=\"label\" translatable=\"yes\">Control maximized windows.</p"
  "roperty><property name=\"visible\">True</property><property name=\"can-"
  "focus\">True</property><property name=\"receives-default\">False</prope"
  "rty><property name=\"draw-indicator\">True</property></object><packing>"
  "<property name=\"expand\">True</property><property name=\"fill\">True</"
  "property><property name=\"position\">0</property></packing></child><chi"
  "ld><object class=\"GtkRadioButton\" id=\"active_window\"><property name"
  "=\"label\" translatable=\"yes\">Control active windows.</property><prop"
  "erty name=\"visible\">True</property><property name=\"can-focus\">True<"
  "/property><property name=\"receives-default\">False</property><property"
  " name=\"draw-indicator\">True</property><property name=\"group\">only_m"
  "aximized</property></object><packing><property name=\"expand\">True</pr"
  "operty><property name=\"fill\">True</property><property name=\"position"
  "\">1</property></packing></child><child><object class=\"GtkCheckButton\""
  " id=\"show_on_desktop\"><property name=\"label\" translatable=\"yes\">S"
  "how the plugin on desktop.</property><property name=\"visible\">True</p"
  "roperty><property name=\"can-focus\">True</property><property name=\"re"
  "ceives-default\">False</property><property name=\"draw-indicator\">True"
  "</property></object><packing><property name=\"expand\">False</property>"
  "<property name=\"fill\">True</property><property name=\"position\">2</p"
  "roperty></packing></child></object></child><child type=\"label\"><objec"
  "t class=\"GtkLabel\" id=\"label1\"><property name=\"visible\">True</pro"
  "perty><property name=\"can-focus\">False</property><property name=\"lab"
  "el\" translatable=\"yes\">&lt;b&gt;Behaviour&lt;/b&gt;</property><prope"
  "rty name=\"use-markup\">True</property></object></child></object><packi"
  "ng><property name=\"expand\">True</property><property name=\"fill\">Tru"
  "e</property><property name=\"position\">0</property></packing></child><"
  "child><object class=\"GtkFrame\" id=\"contentarea2\"><property name=\"v"
  "isible\">True</property><property name=\"can-focus\">False</property><p"
  "roperty name=\"label-xalign\">0</property><property name=\"shadow-type\""
  ">none</property><child><object class=\"GtkBox\" id=\"vbox2\"><property "
  "name=\"visible\">True</property><property name=\"can-focus\">False</pro"
  "perty><property name=\"margin-start\">12</property><property name=\"mar"
  "gin-end\">10</property><property name=\"margin-top\">5</property><prope"
  "rty name=\"margin-bottom\">3</property><property name=\"orientation\">v"
  "ertical</property><property name=\"spacing\">6</property><child><object"
  " class=\"GtkBox\" id=\"hbox1\"><property name=\"visible\">True</propert"
  "y><property name=\"can-focus\">False</property><property name=\"spacing"
  "\">5</property><child><object class=\"GtkLabel\" id=\"width_label\"><pr"
  "operty name=\"visible\">True</property><property name=\"can-focus\">Fal"
  "se</property><property name=\"xpad\">2</property><property name=\"label"
  "\" translatable=\"yes\">Width:</property><property name=\"xalign\">0</p"
  "roperty><attributes><attribute name=\"weight\" value=\"normal\"/></attr"
  "ibutes></object><packing><property name=\"expand\">False</property><pro"
  "perty name=\"fill\">True</property><property name=\"position\">0</prope"
  "rty></packing></child><child><object class=\"GtkComboBox\" id=\"size_mo"
  "de\"><property name=\"visible\">True</property><property name=\"can-foc"
  "us\">False</property><property name=\"model\">size_mode_list</property>"
  "<child><object class=\"GtkCellRendererText\" id=\"cellrenderertext\"/><"
  "attributes><attribute name=\"text\">0</attribute></attributes></child><"
  "/object><packing><property name=\"expand\">False</property><property na"
  "me=\"fill\">True</property><property name=\"position\">1</property></pa"
  "cking></child><child><object class=\"GtkSpinButton\" id=\"titlesize\"><"
  "property name=\"visible\">True</property><property name=\"can-focus\">T"
  "rue</property><property name=\"invisible-char\">\342\200\242</property>"
  "<property name=\"width-chars\">3</property><property name=\"primary-ico"
  "n-activatable\">False</property><property name=\"secondary-icon-activat"
  "able\">False</property><property name=\"snap-to-ticks\">True</property>"
  "<property name=\"numeric\">True</property></object><packing><property n"
  "ame=\"expand\">False</property><property name=\"fill\">True</property><"
  "property name=\"position\">2</property></packing></child><child><object"
  " class=\"GtkLabel\" id=\"width_unit\"><property name=\"visible\">True</"
  "property><property name=\"can-focus\">False</property><property name=\""
  "label\" translatable=\"yes\">chars</property><property name=\"xalign\">"
  "0</property><attributes><attribute name=\"style\" value=\"italic\"/></a"
  "ttributes></object><packing><property name=\"expand\">False</property><"
  "property name=\"fill\">True</property><property name=\"position\">3</pr"
  "operty></packing></child></object><packing><property name=\"expand\">Tr"
  "ue</property><property name=\"fill\">True</property><property name=\"po"
  "sition\">0</property></packing></child><child><object class=\"GtkBox\" "
  "id=\"hbox2\"><property name=\"visible\">True</property><property name=\""
  "can-focus\">False</property><property name=\"spacing\">5</property><chi"
  "ld><object class=\"GtkLabel\" id=\"title_padding_label\"><property name"
  "=\"visible\">True</property><property name=\"can-focus\">False</propert"
  "y><property name=\"xpad\">2</property><property name=\"label\" translat"
  "able=\"yes\">Padding:</property><property name=\"xalign\">0</property><"
  "/object><packing><property name=\"expand\">False</property><property na"
  "me=\"fill\">True</property><property name=\"position\">0</property></pa"
  "cking></child><child><object class=\"GtkSpinButton\" id=\"title_padding"
  "\"><property name=\"visible\">True</property><property name=\"can-focus"
  "\">True</property><property name=\"invisible-char\">\342\200\242</prope"
  "rty><property name=\"width-chars\">2</property><property name=\"primary"
  "-icon-activatable\">False</property><property name=\"secondary-icon-act"
  "ivatable\">False</property><property name=\"snap-to-ticks\">True</prope"
  "rty><property name=\"numeric\">True</property></object><packing><proper"
  "ty name=\"expand\">False</property><property name=\"fill\">True</proper"
  "ty><property name=\"position\">1</property></packing></child><child><ob"
  "ject class=\"GtkLabel\" id=\"padding_unit\"><property name=\"visible\">"
  "True</property><property name=\"can-focus\">False</property><property n"
  "ame=\"label\" translatable=\"yes\">pixels</property><property name=\"xa"
  "lign\">0</property><attributes><attribute name=\"style\" value=\"italic"
  "\"/></attributes></object><packing><property name=\"expand\">False</pro"
  "perty><property name=\"fill\">True</property><property name=\"position\""
  ">2</property></packing></child></object><packing><property name=\"expan"
  "d\">True</property><property name=\"fill\">True</property><property nam"
  "e=\"position\">1</property></packing></child><child><object class=\"Gtk"
  "CheckButton\" id=\"full_name\"><property name=\"label\" translatable=\""
  "yes\">Show the full title name</property><property name=\"visible\">Tru"
  "e</property><property name=\"can-focus\">True</property><property name="
  "\"receives-default\">False</property><property name=\"draw-indicator\">"
  "True</property></object><packing><property name=\"expand\">True</proper"
  "ty><property name=\"fill\">True</property><property name=\"position\">2"
  "</property></packing></child><child><object class=\"GtkCheckButton\" id"
  "=\"two_lines\"><property name=\"label\" translatable=\"yes\">Display th"
  "e title in two lines</property><property name=\"visible\">True</propert"
  "y><property name=\"can-focus\">True</property><property name=\"receives"
  "-default\">False</property><property name=\"draw-indicator\">True</prop"
  "erty></object><packing><property name=\"expand\">True</property><proper"
  "ty name=\"fill\">True</property><property name=\"position\">3</property"
  "></packing></child><child><object class=\"GtkCheckButton\" id=\"sync_wm"
  "_font\"><property name=\"label\" translatable=\"yes\">Sync the font wit"
  "h the window manager.</property><property name=\"visible\">True</proper"
  "ty><property name=\"can-focus\">True</property><property name=\"receive"
  "s-default\">False</property><property name=\"active\">True</property><p"
  "roperty name=\"draw-indicator\">True</property></object><packing><prope"
  "rty name=\"expand\">False</property><property name=\"fill\">True</prope"
  "rty><property name=\"position\">4</property></packing></child><child><o"
  "bject class=\"GtkGrid\" id=\"grid1\"><property name=\"visible\">True</p"
  "roperty><property name=\"can-focus\">False</property><property name=\"c"
  "olumn-homogeneous\">True</property><child><object class=\"GtkLabel\" id"
  "=\"title_font_label\"><property name=\"visible\">True</property><proper"
  "ty name=\"can-focus\">False</property><property name=\"xpad\">2</proper"
  "ty><property name=\"label\" translatable=\"yes\">Title font:</property>"
  "<property name=\"xalign\">0</property><attributes><attribute name=\"wei"
  "ght\" value=\"normal\"/></attributes></object><packing><property name=\""
  "left-attach\">0</property><property name=\"top-attach\">0</property></p"
  "acking></child><child><object class=\"GtkFontButton\" id=\"title_font\""
  "><property name=\"visible\">True</property><property name=\"can-focus\""
  ">True</property><property name=\"receives-default\">True</property><pro"
  "perty name=\"font\">Sans 12</property></object><packing><property name="
  "\"left-attach\">1</property><property name=\"top-attach\">0</property><"
  "/packing></child><child><object class=\"GtkLabel\" id=\"subtitle_font_l"
  "abel\"><property name=\"visible\">True</property><property name=\"can-f"
  "ocus\">False</property><property name=\"xpad\">2</property><property na"
  "me=\"label\" translatable=\"yes\">Subtitle font:</property><property na"
  "me=\"xalign\">0</property><attributes><attribute name=\"weight\" value="
  "\"normal\"/></attributes></object><packing><property name=\"left-attach"
  "\">0</property><property name=\"top-attach\">1</property></packing></ch"
  "ild><child><object class=\"GtkFontButton\" id=\"subtitle_font\"><proper"
  "ty name=\"visible\">True</property><property name=\"can-focus\">True</p"
  "roperty><property name=\"receives-default\">True</property><property na"
  "me=\"font\">Sans 12</property></object><packing><property name=\"left-a"
  "ttach\">1</property><property name=\"top-attach\">1</property></packing"
  "></child><child><object class=\"GtkLabel\" id=\"title_alignment_label\""
  "><property name=\"visible\">True</property><property name=\"can-focus\""
  ">False</property><property name=\"xpad\">2</property><property name=\"l"
  "abel\" translatable=\"yes\">Alignment:</property><property name=\"xalig"
  "n\">0</property></object><packing><property name=\"left-attach\">0</pro"
  "perty><property name=\"top-attach\">2</property></packing></child><chil"
  "d><object class=\"GtkComboBox\" id=\"title_alignment\"><property name=\""
  "visible\">True</property><property name=\"can-focus\">False</property><"
  "property name=\"model\">title_alignment_list</property><child><object c"
  "lass=\"GtkCellRendererText\" id=\"cellrenderertext1\"/><attributes><att"
  "ribute name=\"text\">0</attribute></attributes></child></object><packin"
  "g><property name=\"left-attach\">1</property><property name=\"top-attac"
  "h\">2</property></packing></child></object><packing><property name=\"ex"
  "pand\">True</property><property name=\"fill\">True</property><property "
  "name=\"position\">5</property></packing></child></object></child><child"
  " type=\"label\"><object class=\"GtkLabel\" id=\"label2\"><property name"
  "=\"visible\">True</property><property name=\"can-focus\">False</propert"
  "y><property name=\"label\" translatable=\"yes\">&lt;b&gt;Appearance&lt;"
  "/b&gt;</property><property name=\"use-markup\">True</property></object>"
  "</child></object><packing><property name=\"expand\">True</property><pro"
  "perty name=\"fill\">True</property><property name=\"position\">1</prope"
  "rty></packing></child></object></interface>"
};

static const unsigned windowck_dialogs_ui_length = 12807u;


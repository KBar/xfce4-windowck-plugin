# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Nick Schermer <nick@xfce.org>, 2022
# Besnik Bleta <besnik@programeshqip.org>, 2022
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-09-05 00:54+0200\n"
"PO-Revision-Date: 2022-04-20 11:58+0000\n"
"Last-Translator: Besnik Bleta <besnik@programeshqip.org>, 2022\n"
"Language-Team: Albanian (https://www.transifex.com/xfce/teams/16840/sq/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: sq\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../common/wck-plugin.c:96
msgid "_Refresh"
msgstr "_Rifreskoje"

#: ../common/wck-plugin.c:197
msgid "Help"
msgstr "Ndihmë"

#: ../common/wck-plugin.c:198
msgid "_Close"
msgstr "_Mbylle"

#: ../common/wck-plugin.c:237
#, c-format
msgid "Unable to open the following url: %s"
msgstr "S’arrihet të hapet url-ja vijuese: %s"

#: ../panel-plugin/buttons/wckbuttons-dialogs.c:308
msgid "Directory"
msgstr "Drejtori"

#: ../panel-plugin/buttons/wckbuttons-dialogs.c:310
msgid "Themes usable"
msgstr "Tema të përdorshme"

#: ../panel-plugin/buttons/wckbuttons-dialogs.glade.h:1
#: ../panel-plugin/menu/wckmenu-dialogs.glade.h:1
#: ../panel-plugin/title/windowck-dialogs.glade.h:7
msgid "Control maximized windows."
msgstr "Kontrolloni dritare të maksimizuara."

#: ../panel-plugin/buttons/wckbuttons-dialogs.glade.h:2
#: ../panel-plugin/menu/wckmenu-dialogs.glade.h:2
#: ../panel-plugin/title/windowck-dialogs.glade.h:8
msgid "Control active windows."
msgstr "Kontrolloni dritare aktive."

#: ../panel-plugin/buttons/wckbuttons-dialogs.glade.h:3
msgid "Show the buttons on desktop."
msgstr "Shfaqi butonat në desktop."

#: ../panel-plugin/buttons/wckbuttons-dialogs.glade.h:4
msgid "Logout on close button desktop."
msgstr "Bëj daljen, kur shtypet butoni i mbylljes në desktop."

#: ../panel-plugin/buttons/wckbuttons-dialogs.glade.h:5
#: ../panel-plugin/menu/wckmenu-dialogs.glade.h:4
#: ../panel-plugin/title/windowck-dialogs.glade.h:10
msgid "<b>Behaviour</b>"
msgstr "<b>Sjellje</b>"

#: ../panel-plugin/buttons/wckbuttons-dialogs.glade.h:6
msgid "Get in sync with the window manager theme."
msgstr "Njëkohësoje me temën e përgjegjësit të dritareve."

#: ../panel-plugin/buttons/wckbuttons-dialogs.glade.h:7
msgid "Button layout:"
msgstr "Skemë butonash:"

#: ../panel-plugin/buttons/wckbuttons-dialogs.glade.h:8
msgid ""
"Put the buttons id in the desired order.\n"
"Example: [HMC]\n"
"H=Hide, M=Maximize/unMaximize, C=Close"
msgstr ""
"Vendosini ID-të e butonave në radhën e dëshiruar.\n"
"Shembull: [HMC]\n"
"H=Fshihe, M=Maksimizoje/Çmaksimizoje, C=Mbylle"

#: ../panel-plugin/buttons/wckbuttons-dialogs.glade.h:11
#: ../panel-plugin/menu/wckmenu-dialogs.glade.h:7
#: ../panel-plugin/title/windowck-dialogs.glade.h:22
msgid "<b>Appearance</b>"
msgstr "<b>Dukje</b>"

#: ../panel-plugin/buttons/wckbuttons.desktop.in.h:1
msgid "Window Header - Buttons"
msgstr "Krye Dritareje - Butona"

#: ../panel-plugin/buttons/wckbuttons.desktop.in.h:2
msgid "Put the maximized window buttons on the panel."
msgstr "Butonat e dritares së maksimizuar vendosi te paneli."

#: ../panel-plugin/menu/wckmenu-dialogs.glade.h:3
#: ../panel-plugin/title/windowck-dialogs.glade.h:9
msgid "Show the plugin on desktop."
msgstr "Shfaqe shtojcën në desktop."

#: ../panel-plugin/menu/wckmenu-dialogs.glade.h:5
msgid "Show the window Icon."
msgstr "Shfaq Ikonën e dritareve."

#: ../panel-plugin/menu/wckmenu-dialogs.glade.h:6
msgid "Menu"
msgstr "Menu"

#: ../panel-plugin/menu/wckmenu-plugin.desktop.in.h:1
msgid "Window Header - Menu"
msgstr "Krye Dritareje - Menu"

#: ../panel-plugin/menu/wckmenu-plugin.desktop.in.h:2
msgid "Put the maximized window menu on the panel."
msgstr "Vëre menunë e maksimizuar të dritare te paneli."

#: ../panel-plugin/title/windowck-dialogs.glade.h:1
msgid "at most"
msgstr "e shumta"

#: ../panel-plugin/title/windowck-dialogs.glade.h:2
msgid "fixed to"
msgstr "fiksuar në"

#: ../panel-plugin/title/windowck-dialogs.glade.h:3
msgid "expand"
msgstr "zgjeroje"

#: ../panel-plugin/title/windowck-dialogs.glade.h:4
msgid "left"
msgstr "majtas"

#: ../panel-plugin/title/windowck-dialogs.glade.h:5
msgid "center"
msgstr "në qendër"

#: ../panel-plugin/title/windowck-dialogs.glade.h:6
msgid "right"
msgstr "djathtas"

#: ../panel-plugin/title/windowck-dialogs.glade.h:11
msgid "Width:"
msgstr "Gjerësi:"

#: ../panel-plugin/title/windowck-dialogs.glade.h:12
msgid "chars"
msgstr "shenja"

#: ../panel-plugin/title/windowck-dialogs.glade.h:13
msgid "Padding:"
msgstr "Mbushje:"

#: ../panel-plugin/title/windowck-dialogs.glade.h:14
msgid "pixels"
msgstr "piksela"

#: ../panel-plugin/title/windowck-dialogs.glade.h:15
msgid "Show the full title name"
msgstr "Shfaq emër të plotë titulli"

#: ../panel-plugin/title/windowck-dialogs.glade.h:16
msgid "Display the title in two lines"
msgstr "Shfaqe titullin në dy rreshta"

#: ../panel-plugin/title/windowck-dialogs.glade.h:17
msgid "Sync the font with the window manager."
msgstr "Njëkohëso shkronjat me përgjegjësin e dritareve."

#: ../panel-plugin/title/windowck-dialogs.glade.h:18
msgid "Title font:"
msgstr "Shkronja titulli:"

#: ../panel-plugin/title/windowck-dialogs.glade.h:19
msgid "Subtitle font:"
msgstr "Shkronja nëntitulli:"

#: ../panel-plugin/title/windowck-dialogs.glade.h:20
msgid "Alignment:"
msgstr "Drejtim:"

#: ../panel-plugin/title/windowck-dialogs.glade.h:21
msgid "Title"
msgstr "Titull"

#: ../panel-plugin/title/windowck-plugin.desktop.in.h:1
msgid "Window Header - Title"
msgstr "Krye Dritareje - Titull"

#: ../panel-plugin/title/windowck-plugin.desktop.in.h:2
msgid "Put the maximized window title on the panel."
msgstr "Titullin e dritares së maksimizuar vendose te paneli."

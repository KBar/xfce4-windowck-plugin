# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Xfce Bot <transifex@xfce.org>, 2022
# Michal Várady <miko.vaji@gmail.com>, 2022
# Nick Schermer <nick@xfce.org>, 2022
# fri, 2022
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-09-05 00:54+0200\n"
"PO-Revision-Date: 2022-04-20 11:58+0000\n"
"Last-Translator: fri, 2022\n"
"Language-Team: Czech (https://www.transifex.com/xfce/teams/16840/cs/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: cs\n"
"Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;\n"

#: ../common/wck-plugin.c:96
msgid "_Refresh"
msgstr "_Obnovit"

#: ../common/wck-plugin.c:197
msgid "Help"
msgstr "Nápověda"

#: ../common/wck-plugin.c:198
msgid "_Close"
msgstr "_Zavřít"

#: ../common/wck-plugin.c:237
#, c-format
msgid "Unable to open the following url: %s"
msgstr "Nelze otevřít následující url: %s"

#: ../panel-plugin/buttons/wckbuttons-dialogs.c:308
msgid "Directory"
msgstr "Složka"

#: ../panel-plugin/buttons/wckbuttons-dialogs.c:310
msgid "Themes usable"
msgstr "Dostupné styly"

#: ../panel-plugin/buttons/wckbuttons-dialogs.glade.h:1
#: ../panel-plugin/menu/wckmenu-dialogs.glade.h:1
#: ../panel-plugin/title/windowck-dialogs.glade.h:7
msgid "Control maximized windows."
msgstr "Ovládat maximalizovaná okna."

#: ../panel-plugin/buttons/wckbuttons-dialogs.glade.h:2
#: ../panel-plugin/menu/wckmenu-dialogs.glade.h:2
#: ../panel-plugin/title/windowck-dialogs.glade.h:8
msgid "Control active windows."
msgstr "Ovládat aktivní okna."

#: ../panel-plugin/buttons/wckbuttons-dialogs.glade.h:3
msgid "Show the buttons on desktop."
msgstr "Zobrazit tlačítka na ploše."

#: ../panel-plugin/buttons/wckbuttons-dialogs.glade.h:4
msgid "Logout on close button desktop."
msgstr "Odhlásit se pomocí tlačítka Zavřít bez okna na ploše."

#: ../panel-plugin/buttons/wckbuttons-dialogs.glade.h:5
#: ../panel-plugin/menu/wckmenu-dialogs.glade.h:4
#: ../panel-plugin/title/windowck-dialogs.glade.h:10
msgid "<b>Behaviour</b>"
msgstr "<b>Chování</b>"

#: ../panel-plugin/buttons/wckbuttons-dialogs.glade.h:6
msgid "Get in sync with the window manager theme."
msgstr "Synchronizovat se stylem Správce oken."

#: ../panel-plugin/buttons/wckbuttons-dialogs.glade.h:7
msgid "Button layout:"
msgstr "Rozložení tlačítek:"

#: ../panel-plugin/buttons/wckbuttons-dialogs.glade.h:8
msgid ""
"Put the buttons id in the desired order.\n"
"Example: [HMC]\n"
"H=Hide, M=Maximize/unMaximize, C=Close"
msgstr ""
"Vložte id tlačítek v očekávaném pořadí.\n"
"Příklad: [HMC]\n"
"H=Minimalizovat, M=Maximalizovat/Obnovit, C=Zavřít"

#: ../panel-plugin/buttons/wckbuttons-dialogs.glade.h:11
#: ../panel-plugin/menu/wckmenu-dialogs.glade.h:7
#: ../panel-plugin/title/windowck-dialogs.glade.h:22
msgid "<b>Appearance</b>"
msgstr "<b>Vzhled</b>"

#: ../panel-plugin/buttons/wckbuttons.desktop.in.h:1
msgid "Window Header - Buttons"
msgstr "Hlavička okna - Tlačítka"

#: ../panel-plugin/buttons/wckbuttons.desktop.in.h:2
msgid "Put the maximized window buttons on the panel."
msgstr "Vloží tlačítka pro ovládání okna do panelu."

#: ../panel-plugin/menu/wckmenu-dialogs.glade.h:3
#: ../panel-plugin/title/windowck-dialogs.glade.h:9
msgid "Show the plugin on desktop."
msgstr "Zobrazit na ploše."

#: ../panel-plugin/menu/wckmenu-dialogs.glade.h:5
msgid "Show the window Icon."
msgstr "Zobrazit ikonu okna."

#: ../panel-plugin/menu/wckmenu-dialogs.glade.h:6
msgid "Menu"
msgstr "Nabídka"

#: ../panel-plugin/menu/wckmenu-plugin.desktop.in.h:1
msgid "Window Header - Menu"
msgstr ""

#: ../panel-plugin/menu/wckmenu-plugin.desktop.in.h:2
msgid "Put the maximized window menu on the panel."
msgstr ""

#: ../panel-plugin/title/windowck-dialogs.glade.h:1
msgid "at most"
msgstr "nejvíce"

#: ../panel-plugin/title/windowck-dialogs.glade.h:2
msgid "fixed to"
msgstr "přesně"

#: ../panel-plugin/title/windowck-dialogs.glade.h:3
msgid "expand"
msgstr "roztáhnout"

#: ../panel-plugin/title/windowck-dialogs.glade.h:4
msgid "left"
msgstr "vlevo"

#: ../panel-plugin/title/windowck-dialogs.glade.h:5
msgid "center"
msgstr "uprostřed"

#: ../panel-plugin/title/windowck-dialogs.glade.h:6
msgid "right"
msgstr "vpravo"

#: ../panel-plugin/title/windowck-dialogs.glade.h:11
msgid "Width:"
msgstr "Šířka:"

#: ../panel-plugin/title/windowck-dialogs.glade.h:12
msgid "chars"
msgstr "znaků"

#: ../panel-plugin/title/windowck-dialogs.glade.h:13
msgid "Padding:"
msgstr "Odsazení:"

#: ../panel-plugin/title/windowck-dialogs.glade.h:14
msgid "pixels"
msgstr "pixelů"

#: ../panel-plugin/title/windowck-dialogs.glade.h:15
msgid "Show the full title name"
msgstr "Zobrazit plný text titulku"

#: ../panel-plugin/title/windowck-dialogs.glade.h:16
msgid "Display the title in two lines"
msgstr "Zobrazit titulek ve dvou řádcích"

#: ../panel-plugin/title/windowck-dialogs.glade.h:17
msgid "Sync the font with the window manager."
msgstr "Synchronizovat písmo se Správcem oken."

#: ../panel-plugin/title/windowck-dialogs.glade.h:18
msgid "Title font:"
msgstr "Písmo titulku:"

#: ../panel-plugin/title/windowck-dialogs.glade.h:19
msgid "Subtitle font:"
msgstr "Písmo podtitulku:"

#: ../panel-plugin/title/windowck-dialogs.glade.h:20
msgid "Alignment:"
msgstr "Zarovnání:"

#: ../panel-plugin/title/windowck-dialogs.glade.h:21
msgid "Title"
msgstr "Titulek"

#: ../panel-plugin/title/windowck-plugin.desktop.in.h:1
msgid "Window Header - Title"
msgstr "Hlavička okna - Titulek"

#: ../panel-plugin/title/windowck-plugin.desktop.in.h:2
msgid "Put the maximized window title on the panel."
msgstr "Vloží titulek maximalizovaných oken do panelu."
